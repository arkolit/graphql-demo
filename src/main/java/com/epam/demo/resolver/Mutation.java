package com.epam.demo.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.epam.demo.exception.BookNotFoundException;
import com.epam.demo.model.Author;
import com.epam.demo.model.Book;
import com.epam.demo.repository.AuthorRepo;
import com.epam.demo.repository.BookRepo;
import org.springframework.stereotype.Service;

@Service
public class Mutation implements GraphQLMutationResolver {

    private BookRepo bookRepo;
    private AuthorRepo authorRepo;

    public Mutation(AuthorRepo authorRepository, BookRepo bookRepository) {
        this.authorRepo = authorRepository;
        this.bookRepo = bookRepository;
    }

    public Author newAuthor(String firstName, String lastName) {
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);
        authorRepo.save(author);
        return author;
    }

    public Book newBook(String title, String isbn, Integer pageCount, Long authorId) {
        Book book = new Book();
        book.setAuthor(Author.builder().id(authorId).build());
        book.setTitle(title);
        book.setIsbn(isbn);
        book.setPageCount(pageCount != null ? pageCount : 0);
        bookRepo.save(book);
        return book;
    }

    public boolean deleteBook(Long id) {
        bookRepo.delete(id);
        return true;
    }

    public Book updateBookPageCount(Integer pageCount, Long id) {
        Book book = bookRepo.findOne(id);
        if (book == null) {
            throw new BookNotFoundException("The book to be updated was not found", id);
        }
        book.setPageCount(pageCount);
        bookRepo.save(book);
        return book;
    }
}