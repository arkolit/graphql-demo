package com.epam.demo.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.epam.demo.model.Author;
import com.epam.demo.model.Book;
import com.epam.demo.repository.AuthorRepo;
import com.epam.demo.repository.BookRepo;
import org.springframework.stereotype.Service;

@Service
public class Query implements GraphQLQueryResolver {

    private BookRepo bookRepo;
    private AuthorRepo authorRepo;

    public Query(AuthorRepo authorRepo, BookRepo bookRepo) {
        this.authorRepo = authorRepo;
        this.bookRepo = bookRepo;
    }

    public Iterable<Book> findAllBooks() {
        return bookRepo.findAll();
    }

    public Iterable<Author> findAllAuthors() {
        return authorRepo.findAll();
    }

    public long countBooks() {
        return bookRepo.count();
    }

    public long countAuthors() {
        return authorRepo.count();
    }
}