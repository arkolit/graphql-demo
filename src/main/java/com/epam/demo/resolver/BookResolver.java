package com.epam.demo.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.epam.demo.model.Author;
import com.epam.demo.model.Book;
import com.epam.demo.repository.AuthorRepo;
import org.springframework.stereotype.Service;

@Service
public class BookResolver implements GraphQLResolver<Book> {

    private AuthorRepo authorRepo;

    public BookResolver(AuthorRepo authorRepo) {
        this.authorRepo = authorRepo;
    }

    public Author getAuthor(Book book) {
        return authorRepo.findOne(book.getAuthor().getId());
    }
}